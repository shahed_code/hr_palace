<html>
<head>
<title>Reservation</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/body.css" type="text/css">
<link rel="stylesheet" href="prgaph.css" type="text/css">
<link rel="stylesheet" href="css/link.css" type="text/css">
</head>

<body bgcolor="#FFFFFF" text="#C6FFC6" leftmargin="0" topmargin="0">
<table width="881" border="0" align="center" cellpadding="0" cellspacing="0" height="514" bgcolor="#FDF5D5">
  <tr> 
    <td colspan="2" height="32" bgcolor="#AB910C"> 
      <div align="center"> 
        <?php
	  	include"mnu_top.php";
	  ?>
      </div>
    </td>
  </tr>
  <tr> 
    <td colspan="2" class="body"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="15%" bgcolor="#FFFFFF"><img src="images/logo_rp.gif" width="193" height="89"></td>
          <td width="85%" valign="middle" bgcolor="#FFFFFF"> 
            <div align="right"><img src="images/wecare.jpg" width="317" height="36"></div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td valign="top" height="33">
      <table width="100%" border="1" cellspacing="0" cellpadding="0" align="left" bordercolor="#999900">
        <tr> 
          <td height="27" bgcolor="#AB910C"><font color="#FFFFFF" size="3" face="Arial, Helvetica, sans-serif"><b><i>&nbsp;Reserve 
            Hotel</i></b></font> </td>
        </tr>
        <tr> 
          <td height="162">
		    <form action= confirm_reserve.php name=resform method="get">
              <table width="100%" border="1" cellpadding="0" align="center" bordercolor="#FFFFFF">
                <tr> 
                  <td width="7%" bgcolor="#574906" height="24"> 
                    <div align="center"><b><font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">1</font></b></div>
                  </td>
                  <td width="93%" bgcolor="#AB910C" height="24" class="pgraph"> 
                    <b>&nbsp;&nbsp;Enter Dates</b></td>
                </tr>
                <tr> 
                  <td width="7%">&nbsp;</td>
                  <td width="93%"> 
                    <table width="63%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td colspan="4" class="pgraph" height="42"> 
                          <div align="left"><b>Check in Dates</b></div>
                        </td>
                      </tr>
                      <tr> 
                        <td class="pgraph" width="10%">Date </td>
                        <td class="pgraph" width="33%"> 
                          <select name="chkdate">
                            <option value="0" selected>Select ....</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                          </select>
                        </td>
                        <td class="pgraph" width="11%">Month </td>
                        <td class="pgraph" width="46%"> 
                          <select name="mntdate">
                            <option value="0" selected>Select .....</option>
                            <option value="January">January</option>
                            <option value="February">February</option>
                            <option value="March">March</option>
                            <option value="April">April</option>
                            <option value="May">May</option>
                            <option value="June">June</option>
                            <option value="July">July</option>
                            <option value="August">August</option>
                            <option value="September">September</option>
                            <option value="October">October</option>
                            <option value="November">November</option>
                            <option value="December">December</option>
                          </select>
                        </td>
                      </tr>
                      <tr> 
                        <td colspan="4" class="pgraph" height="40"> 
                          <div align="left"><b>Check out Date</b></div>
                        </td>
                      </tr>
                      <tr> 
                        <td class="pgraph" width="10%">Date </td>
                        <td class="pgraph" width="33%"> 
                          <select name="outdate">
                            <option value="0" selected>Select ....</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                          </select>
                        </td>
                        <td class="pgraph" width="11%">Month </td>
                        <td class="pgraph" width="46%"> 
                          <select name="outmonth">
                            <option value="0" selected>Select .....</option>
                            <option value="January">January</option>
                            <option value="February">February</option>
                            <option value="March">March</option>
                            <option value="April">April</option>
                            <option value="May">May</option>
                            <option value="June">June</option>
                            <option value="July">July</option>
                            <option value="August">August</option>
                            <option value="September">September</option>
                            <option value="October">October</option>
                            <option value="November">November</option>
                            <option value="December">December</option>
                          </select>
                        </td>
                      </tr>
                      <tr> 
                        <td colspan="4">&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td width="7%" bgcolor="#574906" height="23"> 
                    <div align="center"><b><font color="#FFFFFF" face="Arial, Helvetica, sans-serif" size="2">2</font></b></div>
                  </td>
                  <td width="93%" bgcolor="#AB910C" class="pgraph" height="23"><b>&nbsp;Rooms</b></td>
                </tr>
                <tr> 
                  <td width="7%" height="2">&nbsp;</td>
                  <td width="93%" height="2"> 
                    <table width="94%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td colspan="5">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="pgraph" width="10%">&nbsp;&nbsp;Rooms</td>
                        <td class="pgraph" width="15%"> 
                          <select name="room">
                            <option value="0" selected>Select ....</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                          </select>
                        </td>
                        <td class="pgraph" width="17%"> 
                          <div align="right">Room Type</div>
                        </td>
                        <td class="pgraph" width="51%"> 
                          <select name="rtypes">
                            <option value="0" selected>Select .....</option>
                            <option value="Special Suit">Special Suit</option>
                            <option value="Suit Room">Suit Room</option>
                            <option value="A/C Deluxe Three Bed">A/C Deluxe Three 
                            Bed</option>
                            <option value="A/C Three Bed">A/C Three Bed</option>
                            <option value="A/C Twin Deluxe">A/C Twin Deluxe</option>
                            <option value="A/C Couple Deluxe (Double Bed, Bath-tub)">A/C 
                            Couple Deluxe (Double Bed, Bath-tub)</option>
                            <option value="A/C Standard Double (Two Single Bed)">A/C 
                            Standard Double (Two Single Bed)</option>
                            <option value="A/C Single Room">A/C Single Room</option>
                            <option value="Non A/C Double Room ">Non A/C Double 
                            Room </option>
                            <option value="Non A/C Single Room ">Non A/C Single 
                            Room </option>
                          </select>
                        </td>
                        <td class="pgraph" width="7%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="5">&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td width="7%" bgcolor="#574906" height="20"> 
                    <div align="center"><b><font size="2" face="Arial, Helvetica, sans-serif" color="#FFFFFF">3</font></b></div>
                  </td>
                  <td width="93%" height="20" bgcolor="#AB910C"><b> &nbsp;<span class="pgraph">Gusets</span></b></td>
                </tr>
                <tr> 
                  <td width="7%" height="2">&nbsp;</td>
                  <td width="93%" height="2"> 
                    <table width="94%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td colspan="5">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="pgraph" width="16%">&nbsp;&nbsp;Adults</td>
                        <td class="pgraph" width="16%"> 
                          <select name="adults">
                            <option value="0" selected>Select ....</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                          </select>
                        </td>
                        <td class="pgraph" width="11%">Children</td>
                        <td class="pgraph" width="21%">
                          <select name="children">
                            <option value="0" selected>Select ....</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                          </select>
                        </td>
                        <td class="pgraph" width="36%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="5">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="5">&nbsp; </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td width="7%" height="23" bgcolor="#574906"> 
                    <div align="center"><b><font size="2" face="Arial, Helvetica, sans-serif" color="#FFFFFF">4</font></b></div>
                  </td>
                  <td width="93%" height="23" bgcolor="#AB910C"><b><span class="pgraph">&nbsp;Gusets 
                    Information </span></b></td>
                </tr>
                <tr> 
                  <td width="7%" height="2">&nbsp;</td>
                  <td width="93%" height="2"> 
                    <table width="98%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="18%" class="pgraph">First name</td>
                        <td width="82%"> 
                          <input type="text" name="fname" size="40">
                        </td>
                      </tr>
                      <tr> 
                        <td width="18%" class="pgraph">Last name</td>
                        <td width="82%"> 
                          <input type="text" name="lname" size="40">
                        </td>
                      </tr>
                      <tr> 
                        <td width="18%" class="pgraph" valign="top">Contact Address</td>
                        <td width="82%"> 
                          <textarea name="addr" cols="30" rows="3"></textarea>
                        </td>
                      </tr>
                      <tr> 
                        <td width="18%" class="pgraph">Phone</td>
                        <td width="82%"> 
                          <input type="text" name="ph" size="40">
                        </td>
                      </tr>
                      <tr> 
                        <td width="18%" class="pgraph">E-mail</td>
                        <td width="82%"> 
                          <input type="text" name="em" size="40">
                        </td>
                      </tr>
                      <tr> 
                        <td width="18%" class="pgraph">&nbsp;</td>
                        <td width="82%">&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td width="7%" height="2">&nbsp;</td>
                  <td width="93%" height="2">
                    <input type="submit" name="Submit" value="Reserve">
                  </td>
                </tr>
              </table>
		</form>
          </td>
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top" bgcolor="#E7E1BD"> 
      <div align="center"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#E7E1BD">
          <tr> 
            <td><img src="images/related_link.jpg" width="270" height="31"></td>
          </tr>
          <tr> 
            <td class="pgraph"> &nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              <a href="tariff.php">Tariff</a> </td>
          </tr>
          <tr> 
            <td class="pgraph" height="15">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              <a href="room_suits.php">Rooms &amp; Suits</a></td>
          </tr>
          <tr> 
            <td class="pgraph" height="15">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              <a href="restaurent.php">Restaurent</a></td>
          </tr>
          <tr> 
            <td class="pgraph" height="2">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              <a href="facilities.php">Facility</a></td>
          </tr>
          <tr> 
            <td class="pgraph"><b>&nbsp;</b></td>
          </tr>
        </table>
      </div>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" height="332">
        <tr> 
          <td height="2"> <img src="images/other.jpg" width="270" height="31"></td>
        </tr>
        <tr> 
          <td bgcolor="#E7E1BD" class="pgraph" valign="top" height="234"> <br>
            <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="pgraph">We are always inviting, always innovative, 
                  always interesting. With over 12 years experience in creating 
                  unique moments and special memories for our guests, you can 
                  be sure that when you stay at the HRP, you are in expert hands. 
                  At the HRP you&#146;re at the very heart of the City.</td>
              </tr>
              <tr> 
                <td class="pgraph">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pgraph">At the HRP, it&#146;s our people who are the 
                  heartbeat of this hotel. This Organization has been established 
                  for the employees but not for the profit of its Owner. However, 
                  from another point of view, the employees are the owner. Our 
                  welcoming staff offer exceptional attention to detail, whether 
                  your visit is for conferencing, corporate events, business travel 
                  or leisure.</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td valign="top" height="2" width="70%" bgcolor="#FFFFFF">&nbsp; </td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td colspan="2" class="body"> 
      <div align="center">&copy; 2008-2009 Hotel Royal Palace.</div>
    </td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td colspan="2" class="body" height="2"> 
      <div align="center">Development powered by : <a href="http://www.srishtyinteractive.net?cr=rp" target="_blank">srishty 
        interActive</a></div>
    </td>
  </tr>
</table>
</body>
</html>
