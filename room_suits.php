<html>
<head>
<title>Room &amp; suits</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/body.css" type="text/css">
<link rel="stylesheet" href="prgaph.css" type="text/css">
<link rel="stylesheet" href="css/link.css" type="text/css">
</head>

<body bgcolor="#FFFFFF" text="#C6FFC6" leftmargin="0" topmargin="0">
<table width="881" border="0" align="center" cellpadding="0" cellspacing="0" height="514">
  <tr> 
    <td colspan="2" height="32" bgcolor="#AB910C"> 
      <div align="center"> 
        <?php
	  	include"mnu_top.php";
	  ?>
      </div>
    </td>
  </tr>
  <tr> 
    <td colspan="2" class="body"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="15%"><img src="images/logo_rp.gif" width="193" height="89"></td>
          <td width="85%" valign="middle"> 
            <div align="right"><img src="images/wecare.jpg" width="317" height="36"></div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width="70%" valign="top" height="240"><img src="images/room_t.jpg" width="666" height="241"></td>
    <td rowspan="2" valign="top" bgcolor="#E7E1BD"> 
      <div align="center"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#E7E1BD">
          <tr> 
            <td><img src="images/facility_new.jpg" width="271" height="32"></td>
          </tr>
          <tr> 
            <td class="pgraph"> &nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              Calm &amp; Quiet Atmosphere</td>
          </tr>
          <tr> 
            <td class="pgraph" height="15">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              24 Hours Room Service &amp; Restaurant,</td>
          </tr>
          <tr> 
            <td class="pgraph" height="15">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              24 Hours Lift Service</td>
          </tr>
          <tr> 
            <td class="pgraph" height="2">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              Restaurant At 1st Floor</td>
          </tr>
          <tr> 
            <td class="pgraph" height="8">&nbsp; <img src="images/arrow.gif" width="8" height="7">&nbsp;Direct 
              Dialing System <font size="2">(Local, NWD &amp; ISD)</font></td>
          </tr>
          <tr> 
            <td class="pgraph">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              Fax &amp; Net Surfing Facilities</td>
          </tr>
          <tr> 
            <td class="pgraph">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              Suitable Car Parking</td>
          </tr>
          <tr> 
            <td class="pgraph">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              Standby Generator System</td>
          </tr>
          <tr> 
            <td class="pgraph">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              Group Rates are Available</td>
          </tr>
          <tr> 
            <td class="pgraph">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              Safe Deposit Locker</td>
          </tr>
          <tr> 
            <td class="pgraph">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              Satellite Television</td>
          </tr>
          <tr> 
            <td class="pgraph">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              Hot &amp; Cold Water</td>
          </tr>
          <tr> 
            <td class="pgraph">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              Group Rates Are Available</td>
          </tr>
          <tr> 
            <td class="pgraph">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              Air-Conditioned Conference Room</td>
          </tr>
          <tr> 
            <td class="pgraph">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              Air-Conditioned Barber Shop</td>
          </tr>
          <tr> 
            <td class="pgraph">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              Check-out Time at 12.00 Noon</td>
          </tr>
          <tr> 
            <td class="pgraph" height="2">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              Rent-A-Car Service</td>
          </tr>
          <tr> 
            <td class="pgraph"><b>&nbsp;</b></td>
          </tr>
        </table>
      </div>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" height="332">
        <tr> 
          <td height="2"> <img src="images/other.jpg" width="271" height="32"></td>
        </tr>
        <tr> 
          <td bgcolor="#E7E1BD" class="pgraph" valign="top" height="2"> <br>
            <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="pgraph">We are always inviting, always innovative, 
                  always interesting. With over 12 years experience in creating 
                  unique moments and special memories for our guests, you can 
                  be sure that when you stay at the HRP, you are in expert hands. 
                  At the HRP you&#146;re at the very heart of the City.</td>
              </tr>
              <tr> 
                <td class="pgraph">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pgraph">At the HRP, it&#146;s our people who are the 
                  heartbeat of this hotel. This Organization has been established 
                  for the employees but not for the profit of its Owner. However, 
                  from another point of view, the employees are the owner. Our 
                  welcoming staff offer exceptional attention to detail, whether 
                  your visit is for conferencing, corporate events, business travel 
                  or leisure.</td>
              </tr>
              <tr> 
                <td class="pgraph">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pgraph">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pgraph">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td valign="top" height="255" width="70%">
      <table width="100%" border="0" cellpadding="0" height="166">
        <tr> 
          <td height="31" bgcolor="#AB910C"> 
            <div align="center"><img src="images/rooms.gif" width="270" height="31"></div>
          </td>
        </tr>
        <tr> 
          <td height="82" class="pgraph" bgcolor="#FFFFFF"> 
            <table width="87%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td height="103" width="29%"><img src="images/room_1.jpg" width="154" height="114"></td>
                <td class="pgraph" height="103" rowspan="4" valign="middle" width="41%">The 
                  HRP redefines premium accommodation with a unique blend of space, 
                  comfort, style, service and spectacular views. With our stylish 
                  range of accommodation &#150; 90 furnished rooms &#150; you&#146;re 
                  assured of a rejuvenating stay, whatever your aspirations. Our 
                  spacious rooms are air-conditioned and feature their own level 
                  balconies* .</td>
                <td height="103" width="30%"> 
                  <div align="right"><img src="images/room_2.jpg" width="154" height="114"></div>
                </td>
              </tr>
              <tr> 
                <td width="29%">&nbsp;</td>
                <td width="30%"> 
                  <div align="right"></div>
                </td>
              </tr>
              <tr> 
                <td width="29%"><img src="images/room_3.jpg" width="154" height="114"></td>
                <td width="30%"> 
                  <div align="right"><img src="images/room_4.jpg" width="154" height="114"></div>
                </td>
              </tr>
              <tr> 
                <td width="29%">&nbsp;</td>
                <td width="30%">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td height="2" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td colspan="2" class="body"> 
      <div align="center">&copy; 2008-2009 Hotel Royal Palace.</div>
    </td>
  </tr>
  <tr> 
    <td colspan="2" class="body" height="2"> 
      <div align="center">Development powered by : <a href="http://www.srishtyinteractive.net?cr=rp" target="_blank">srishty 
        interActive</a></div>
    </td>
  </tr>
</table>
</body>
</html>
