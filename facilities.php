<html>
<head>
<title>Facilities</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/body.css" type="text/css">
<link rel="stylesheet" href="prgaph.css" type="text/css">
<link rel="stylesheet" href="css/link.css" type="text/css">
</head>

<body bgcolor="#E7E1BD" text="#000000" leftmargin="0" topmargin="0" link="#990000">
<table width="881" border="0" align="center" cellpadding="0" cellspacing="0" height="514">
  <tr> 
    <td colspan="2" height="32" bgcolor="#AB910C"> 
      <div align="center"> 
        <?php
	  	include"mnu_top.php";
	  ?>
      </div>
    </td>
  </tr>
  <tr> 
    <td colspan="2" class="body"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="15%" bgcolor="#FFFFFF"><img src="images/logo_rp.gif" width="193" height="89"></td>
          <td width="85%" valign="middle" bgcolor="#FFFFFF"> 
            <div align="right"><img src="images/wecare.jpg" width="317" height="36"></div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width="70%" valign="top"><img src="images/facilities.jpg" width="666" height="241"></td>
    <td rowspan="2" valign="top" bgcolor="#FFFFFF"> 
      <div align="center"> </div>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" height="357" bgcolor="#FFFFFF">
        <tr> 
          <td height="2"> <img src="images/other.jpg" width="270" height="31"></td>
        </tr>
        <tr> 
          <td bgcolor="#FFFFFF" class="pgraph" valign="top" height="2"> <br>
            <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF">
              <tr> 
                <td class="pgraph">We are always inviting, always innovative, 
                  always interesting. With over 12 years experience in creating 
                  unique moments and special memories for our guests, you can 
                  be sure that when you stay at the HRP, you are in expert hands. 
                  At the HRP you&#146;re at the very heart of the City.</td>
              </tr>
              <tr> 
                <td class="pgraph">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pgraph">At the HRP, it&#146;s our people who are the 
                  heartbeat of this hotel. This Organization has been established 
                  for the employees but not for the profit of its Owner. However, 
                  from another point of view, the employees are the owner. Our 
                  welcoming staff offer exceptional attention to detail, whether 
                  your visit is for conferencing, corporate events, business travel 
                  or leisure.</td>
              </tr>
              <tr> 
                <td class="pgraph">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pgraph">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pgraph" height="2">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF" class="pgraph" valign="top" height="54"> 
            <table width="94%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
              <tr> 
                <td><img src="images/related_link.jpg" width="270" height="31"></td>
              </tr>
              <tr> 
                <td class="pgraph"> &nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  <a href="tariff.php">Tariff</a> </td>
              </tr>
              <tr> 
                <td class="pgraph" height="15">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  <a href="room_suits.php">Rooms &amp; Suits</a></td>
              </tr>
              <tr> 
                <td class="pgraph" height="17">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  <a href="restaurent.php">Restaurent</a><b>&nbsp;</b></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td valign="top" height="376" width="70%" bgcolor="#FFFFFF"> 
      <table width="100%" border="0" cellpadding="0">
        <tr> 
          <td height="31" bgcolor="#AB910C"> 
            <div align="center"><font face="Garamond" size="5" color="#FFFFFF"><b>Facilities</b></font></div>
          </td>
        </tr>
        <tr> 
          <td height="301" class="pgraph" bgcolor="#FFFFFF"> 
            <table width="80%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" align="center">
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td class="pgraph"> <font size="3">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  Calm &amp; Quiet Atmosphere</font></td>
              </tr>
              <tr> 
                <td class="pgraph" height="15"><font size="3">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  24 Hours Room Service &amp; Restaurant,</font></td>
              </tr>
              <tr> 
                <td class="pgraph" height="15"><font size="3">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  24 Hours Lift Service</font></td>
              </tr>
              <tr> 
                <td class="pgraph" height="2"><font size="3">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  Restaurant At 1st Floor</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font size="3">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  Direct Dialing System (Local, NWD &amp; ISD)</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font size="3">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  Fax &amp; Net Surfing Facilities</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font size="3">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  Suitable Car Parking</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font size="3">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  Standby Generator System</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font size="3">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  Group Rates are Available</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font size="3">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  Safe Deposit Locker</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font size="3">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  Satellite Television</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font size="3">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  Hot &amp; Cold Water</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font size="3">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  Group Rates Are Available</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font size="3">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  Air-Conditioned Conference Room</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font size="3">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  Air-Conditioned Barber Shop</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font size="3">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  Check-out Time at 12.00 Noon</font></td>
              </tr>
              <tr> 
                <td class="pgraph" height="2"><font size="3">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
                  Rent-A-Car Service</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><b>&nbsp;</b></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td height="20" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td colspan="2" class="body" bgcolor="#FFFFFF"> 
      <div align="center">&copy; 2008-2009 Hotel Royal Palace.</div>
    </td>
  </tr>
  <tr> 
    <td colspan="2" class="body" height="2" bgcolor="#FFFFFF"> 
      <div align="center">Development powered by <font color="#000000">: <a href="http://www.srishtyinteractive.net?cr=rp" target="_blank">srishty 
        interActive</a></font></div>
    </td>
  </tr>
</table>
</body>
</html>
