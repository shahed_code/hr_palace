<html>
<head>
<title>Log in</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/body.css" type="text/css">
<link rel="stylesheet" href="prgaph.css" type="text/css">
<link rel="stylesheet" href="css/link.css" type="text/css">
</head>

<body bgcolor="#FDF5D5" text="#000000" leftmargin="0" topmargin="0">
<table width="881" border="0" align="center" cellpadding="0" cellspacing="0" height="347" bgcolor="#FDF5D5">
  <tr> 
    <td colspan="2" height="32" bgcolor="#AB910C"> 
      <div align="center"> 
        <?php
	  	include"mnu_top.php";
	  ?>
      </div>
    </td>
  </tr>
  <tr> 
    <td colspan="2" class="body" height="39"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="15%" bgcolor="#FFFFFF"><img src="images/logo_rp.gif" width="193" height="89"></td>
          <td width="85%" valign="middle" bgcolor="#FFFFFF"> 
            <div align="right"><img src="images/wecare.jpg" width="317" height="36"></div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td valign="top" height="33">
      <table width="100%" border="1" cellspacing="0" cellpadding="0" align="left" bordercolor="#999900" class="pgraph">
        <tr> 
          <td height="26" bgcolor="#AB910C"><font color="#FFFFFF" size="3" face="Arial, Helvetica, sans-serif"><b><i>&nbsp;Log 
            on </i></b></font></td>
        </tr>
        <tr> 
          <td height="162" valign="top" class="pgraph" bgcolor="#FFFFFF"> 
		  <form action=login_check.php method =post>
            <table width="69%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td colspan="4">&nbsp;</td>
              </tr>
              <tr> 
                <td width="30%">&nbsp;</td>
                <td width="55%">&nbsp;</td>
                <td width="7%">&nbsp;</td>
                <td width="8%">&nbsp;</td>
              </tr>
              <tr> 
                <td width="30%"> 
                  <div align="right"><b><font face="Arial, Helvetica, sans-serif" size="2">User 
                    ID</font></b></div>
                </td>
                <td width="55%"> &nbsp;&nbsp; 
                  <input type="text" name="u">
                </td>
                <td width="7%">&nbsp;</td>
                <td width="8%">&nbsp;</td>
              </tr>
              <tr> 
                <td width="30%"> 
                  <div align="right"><b><font face="Arial, Helvetica, sans-serif" size="2">Password</font></b></div>
                </td>
                <td width="55%"> &nbsp;&nbsp; 
                  <input type="password" name="p">
                </td>
                <td width="7%">&nbsp;</td>
                <td width="8%">&nbsp;</td>
              </tr>
              <tr> 
                <td width="30%">&nbsp;</td>
                <td width="55%">&nbsp;</td>
                <td width="7%">&nbsp;</td>
                <td width="8%">&nbsp;</td>
              </tr>
              <tr> 
                <td width="30%">&nbsp;</td>
                <td width="55%"> 
                  <input type="submit" name="Submit" value="Lon in">
                </td>
                <td width="7%">&nbsp;</td>
                <td width="8%">&nbsp;</td>
              </tr>
            </table>
			</form>
          </td>
		  
		  
        </tr>
      </table>
    </td>
    <td rowspan="2" valign="top" bgcolor="#FFFFFF"> 
      <div align="center"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#E7E1BD">
          <tr> 
            <td><img src="images/related_link.jpg" width="270" height="31"></td>
          </tr>
          <tr> 
            <td class="pgraph"> &nbsp;&nbsp;<img src="images/arrow.gif" width="8" height="7"> 
              Check mail</td>
          </tr>
          <tr>
            <td class="pgraph">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              <a href="tariff.php">Tarffi</a></td>
          </tr>
          <tr> 
            <td class="pgraph" height="15">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              <a href="room_suits.php">Rooms &amp; Suits</a></td>
          </tr>
          <tr> 
            <td class="pgraph" height="15">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              <a href="restaurent.php">Restaurant</a></td>
          </tr>
          <tr> 
            <td class="pgraph" height="2">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              <a href="offer.php">New offer</a></td>
          </tr>
          <tr> 
            <td class="pgraph"><b>&nbsp;</b></td>
          </tr>
        </table>
      </div>
      
    </td>
  </tr>
  <tr> 
    <td valign="top" height="2" width="70%" bgcolor="#FFFFFF">&nbsp; </td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td colspan="2" class="body" height="2"> 
      <div align="center">&copy; 2008-2009 Hotel Royal Palace.</div>
    </td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td colspan="2" class="body" height="2"> 
      <div align="center">Development powered by : <a href="http://www.srishtyinteractive.net?cr=rp" target="_blank">srishty 
        interActive</a></div>
    </td>
  </tr>
</table>

</body>
</html>
