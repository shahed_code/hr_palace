<html>
<head>
<title>Restaurent</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/body.css" type="text/css">
<link rel="stylesheet" href="prgaph.css" type="text/css">
<link rel="stylesheet" href="css/link.css" type="text/css">
</head>

<body bgcolor="#FFFFFF" text="#C6FFC6" leftmargin="0" topmargin="0">
<table width="881" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr bgcolor="#AB910C"> 
    <td colspan="2" height="32"> 
      <div align="center"> 
        <?php
	  	include"mnu_top.php";
	  ?>
      </div>
    </td>
  </tr>
  <tr> 
    <td colspan="2" class="body"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="15%"><img src="images/logo_rp.gif" width="193" height="89"></td>
          <td width="85%" valign="middle"> 
            <div align="right"><img src="images/wecare.jpg" width="317" height="36"></div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width="70%"><img src="images/restaurant.jpg" width="666" height="241"></td>
    <td rowspan="2" valign="top"> 
      <div align="center"> 
        <table width="97%" border="0" cellspacing="0" cellpadding="0" bgcolor="#E7E1BD">
          <tr> 
            <td><img src="images/offer_lnk.jpg" width="270" height="31"></td>
          </tr>
          <tr> 
            <td class="pgraph"> <b>&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              Wedding Packages </b></td>
          </tr>
          <tr> 
            <td class="pgraph"><b>&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              Birthday Packages </b></td>
          </tr>
          <tr> 
            <td class="pgraph"><b>&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
              </b></td>
          </tr>
          <tr> 
            <td class="pgraph"><b>&nbsp; <img src="images/arrow.gif" width="8" height="7"></b></td>
          </tr>
          <tr> 
            <td class="pgraph"><b>&nbsp; <img src="images/arrow.gif" width="8" height="7"></b></td>
          </tr>
          <tr> 
            <td class="pgraph"><b></b></td>
          </tr>
          <tr> 
            <td class="pgraph"><b></b></td>
          </tr>
          <tr> 
            <td class="pgraph"><b></b></td>
          </tr>
          <tr> 
            <td class="pgraph"><b></b></td>
          </tr>
          <tr> 
            <td class="pgraph"><b></b></td>
          </tr>
          <tr> 
            <td class="pgraph"><b></b></td>
          </tr>
        </table>
      </div>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td> 
            <table width="96%" border="1" cellspacing="0" cellpadding="0" align="right" bordercolor="#FFFFFF">
              <tr> 
                <td> 
                  <div align="right"></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td valign="top" height="96" width="70%"> 
      <table width="100%" border="0" cellpadding="0">
        <tr> 
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="2" bgcolor="#AB910C" height="28">
            <div align="center"><font face="Times New Roman, Times, serif" size="4" color="#FFFFFF">Restaurant</font></div>
          </td>
        </tr>
        <tr> 
          <td height="31" width="28%" class="pgraph"><img src="images/hall.jpg" width="175" height="172"></td>
          <td height="31" width="72%" class="pgraph">Indulge your taste buds at 
            HRP. Catch up with old friends and new at our restaurant for delectable 
            snacks, or relax in your room with our 24 hrs in-room dining. Whatever 
            your taste, you&#146;ll always find something to tempt you at HRP.</td>
        </tr>
        <tr bgcolor="#AB910C" valign="middle"> 
          <td colspan="2" height="28"> 
            <div align="center"><font face="Times New Roman, Times, serif" size="4" color="#FFFFFF">Our 
              Menu</font></div>
          </td>
        </tr>
        <tr> 
          <td colspan="2" height="74" valign="top"> 
            <table width="72%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td width="20%" height="63"><img src="images/mnu6.jpg" width="114" height="77"></td>
                <td width="20%" height="63" class="pgraph"> 
                  <div align="center"><b>Desi (Bengali)</b></div>
                </td>
                <td width="20%" height="63" class="pgraph"><img src="images/mnu2.jpg" width="114" height="77"></td>
                <td width="16%" height="63" class="pgraph"> 
                  <div align="center"><b>Indian</b></div>
                </td>
                <td width="24%" height="63" class="pgraph"><img src="images/mnu5.jpg" width="114" height="77"></td>
              </tr>
              <tr> 
                <td width="20%" height="6">&nbsp;</td>
                <td height="6" class="pgraph" colspan="4">&nbsp;</td>
              </tr>
              <tr> 
                <td width="20%" height="2"><img src="images/mnu3.jpg" width="114" height="77"></td>
                <td height="2" class="pgraph" width="20%"> 
                  <div align="center"><b>Thai &amp; Chinese</b></div>
                </td>
                <td height="2" class="pgraph" width="20%"><img src="images/mnu7.jpg" width="114" height="77"></td>
                <td height="2" class="pgraph" width="16%"> 
                  <div align="center"><b>Italian</b></div>
                </td>
                <td height="2" class="pgraph" width="24%"><img src="images/mnu1.jpg" width="114" height="77"></td>
              </tr>
              <tr> 
                <td width="20%">&nbsp;</td>
                <td class="pgraph" width="20%"> 
                  <div align="center"></div>
                </td>
                <td class="pgraph" colspan="3">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td colspan="2" height="2">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td colspan="2" class="body"> 
      <div align="center">&copy; 2008-2009 Hotel Royal Palace.</div>
    </td>
  </tr>
  <tr> 
    <td colspan="2" class="body" height="2"> 
      <div align="center">Development powered by : <a href="http://www.srishtyinteractive.net?cr=rp" target="_blank">srishty 
        interActive</a></div>
    </td>
  </tr>
</table>
</body>
</html>
