<html>
<head>
<title>Royal Palace</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/body.css" type="text/css">
<link rel="stylesheet" href="prgaph.css" type="text/css">
</head>

<body bgcolor="#FFF3C6" text="#C6FFC6" leftmargin="0" topmargin="0">
<table width="75%" border="0" align="center" cellpadding="0" cellspacing="0" height="335" bgcolor="#FFFFFF">
  <tr valign="middle"> 
    <td colspan="2" height="37" bgcolor="#AB910C"> 
      <div align="center"> 
        <?php
	  	include"mnu_top.php";
	  ?>
      </div>
    </td>
  </tr>
  <tr> 
    <td colspan="2" class="pgraph"><img src="images/logo_rp.gif" width="193" height="89"></td>
  </tr>
  <tr> 
    <td width="70%"><img src="images/room_t.jpg" width="666" height="241"></td>
    <td width="30%" valign="top"> 
      <table width="98%" border="0" cellspacing="0" cellpadding="0" align="right">
        <tr> 
          <td width="96%" bgcolor="#ab910c"> 
            <div align="center"> 
              <table width="99%" border="0" bgcolor="#AB910C" align="right" cellpadding="0" cellspacing="0">
                <tr> 
                  <td height="30"> 
                    <div align="center"><font face="Arial, Helvetica, sans-serif" size="2"><b><font size="3" color="#000000">Reserve 
                      your room today</font></b></font></div>
                  </td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td> 
                    <table width="62%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td width="39%" class="body"><font size="2" face="Arial, Helvetica, sans-serif" color="#372900" class="body">Arrival 
                          Date</font></td>
                        <td width="61%" class="body"><font size="2" face="Arial, Helvetica, sans-serif" color="#372900" class="body">Departure 
                          Date</font></td>
                      </tr>
                      <tr> 
                        <td width="39%" class="body"> 
                          <input type="text" name="textfield" size="10" maxlength="20">
                        </td>
                        <td width="61%" class="body"> 
                          <input type="text" name="textfield2" size="10" maxlength="20">
                        </td>
                      </tr>
                      <tr> 
                        <td width="39%" class="body"><font size="2" face="Arial, Helvetica, sans-serif" color="#372900" class="body">Rooms</font></td>
                        <td width="61%" class="body"> <font size="2" face="Arial, Helvetica, sans-serif"> 
                          <select name="select">
                            <option value="0" selected><font size="2" face="Arial, Helvetica, sans-serif">Select 
                            .......</font></option>
                            <option value="1"><font size="2" face="Arial, Helvetica, sans-serif">1</font></option>
                            <option value="2"><font size="2" face="Arial, Helvetica, sans-serif">2</font></option>
                            <option value="3"><font size="2" face="Arial, Helvetica, sans-serif">3</font></option>
                            <option value="4"><font size="2" face="Arial, Helvetica, sans-serif">4</font></option>
                            <option value="5"><font size="2" face="Arial, Helvetica, sans-serif">5</font></option>
                            <option value="6"><font size="2" face="Arial, Helvetica, sans-serif">6</font></option>
                            <option value="7"><font size="2" face="Arial, Helvetica, sans-serif">7</font></option>
                            <option value="8"><font size="2" face="Arial, Helvetica, sans-serif">8</font></option>
                            <option value="9"><font size="2" face="Arial, Helvetica, sans-serif">9</font></option>
                            <option value="10"><font size="2" face="Arial, Helvetica, sans-serif">10</font></option>
                          </select>
                          </font></td>
                      </tr>
                      <tr> 
                        <td width="39%" class="body"><font size="2" face="Arial, Helvetica, sans-serif" color="#372900" class="body">Adult 
                          per room</font></td>
                        <td width="61%" class="body"><font size="2" face="Arial, Helvetica, sans-serif" color="#372900" class="body">Crildren 
                          per room</font></td>
                      </tr>
                      <tr> 
                        <td width="39%"> <font size="2" face="Arial, Helvetica, sans-serif"> 
                          <select name="select2">
                            <option value="0" selected><font size="2" face="Arial, Helvetica, sans-serif">Select 
                            .......</font></option>
                            <option value="1"><font size="2" face="Arial, Helvetica, sans-serif">1</font></option>
                            <option value="2"><font size="2" face="Arial, Helvetica, sans-serif">2</font></option>
                            <option value="3"><font size="2" face="Arial, Helvetica, sans-serif">3</font></option>
                            <option value="4"><font size="2" face="Arial, Helvetica, sans-serif">4</font></option>
                            <option value="5"><font size="2" face="Arial, Helvetica, sans-serif">5</font></option>
                            <option value="6"><font size="2" face="Arial, Helvetica, sans-serif">6</font></option>
                            <option value="7"><font size="2" face="Arial, Helvetica, sans-serif">7</font></option>
                            <option value="8"><font size="2" face="Arial, Helvetica, sans-serif">8</font></option>
                            <option value="9"><font size="2" face="Arial, Helvetica, sans-serif">9</font></option>
                            <option value="10"><font size="2" face="Arial, Helvetica, sans-serif">10</font></option>
                          </select>
                          </font></td>
                        <td width="61%"> <font size="2" face="Arial, Helvetica, sans-serif"> 
                          <select name="select3">
                            <option value="0" selected><font size="2" face="Arial, Helvetica, sans-serif">Select 
                            .......</font></option>
                            <option value="1"><font size="2" face="Arial, Helvetica, sans-serif">1</font></option>
                            <option value="2"><font size="2" face="Arial, Helvetica, sans-serif">2</font></option>
                            <option value="3"><font size="2" face="Arial, Helvetica, sans-serif">3</font></option>
                            <option value="4"><font size="2" face="Arial, Helvetica, sans-serif">4</font></option>
                            <option value="5"><font size="2" face="Arial, Helvetica, sans-serif">5</font></option>
                            <option value="6"><font size="2" face="Arial, Helvetica, sans-serif">6</font></option>
                            <option value="7"><font size="2" face="Arial, Helvetica, sans-serif">7</font></option>
                            <option value="8"><font size="2" face="Arial, Helvetica, sans-serif">8</font></option>
                            <option value="9"><font size="2" face="Arial, Helvetica, sans-serif">9</font></option>
                            <option value="10"><font size="2" face="Arial, Helvetica, sans-serif">10</font></option>
                          </select>
                          </font></td>
                      </tr>
                      <tr> 
                        <td width="39%">&nbsp;</td>
                        <td width="61%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="39%" height="2">&nbsp;</td>
                        <td width="61%" height="2"> 
                          <input type="submit" name="Submit" value="Reserve">
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td height="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
              </table>
            </div>
          </td>
          <td width="4%" bgcolor="#AB910C"><img src="images/bar.jpg" width="4" height="241"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td valign="top" height="191" width="70%"> 
      <table width="100%" border="0">
        <tr> 
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td bgcolor="#AB910C"><font face="Garamond" size="5" color="#FFFFFF"><b>Room 
            Tariff </b></font></td>
        </tr>
        <tr> 
          <td height="2">&nbsp;</td>
        </tr>
        <tr> 
          <td height="103"> 
            <table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="pgraph"><font color="#000000"><img src="images/arrow.gif" width="8" height="7"> 
                  Special Suit</font></td>
                <td class="fnt"><font color="#000000">US $ 62</font></td>
                <td class="fnt"><font color="#000000">tk. 4,000/-</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font color="#000000"><img src="images/arrow.gif" width="8" height="7"> 
                  Suit Room </font></td>
                <td class="fnt"><font color="#000000">US $ 54</font></td>
                <td class="fnt"><font color="#000000">tk. 3,500/-</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font color="#000000"><img src="images/arrow.gif" width="8" height="7"> 
                  A/C Deluxe Three Bed</font></td>
                <td class="fnt"><font color="#000000">US $ 39</font></td>
                <td class="fnt"><font color="#000000">tk. 2,500/-</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font color="#000000"><img src="images/arrow.gif" width="8" height="7"> 
                  A/C Three Bed</font></td>
                <td class="fnt"><font color="#000000">US $ 34</font></td>
                <td class="fnt"><font color="#000000">tk. 2,200/-</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font color="#000000"><img src="images/arrow.gif" width="8" height="7"> 
                  A/C Twin Deluxe</font></td>
                <td class="fnt"><font color="#000000">US $ 31</font></td>
                <td class="fnt"><font color="#000000">tk. 2,000/-</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font color="#000000"><img src="images/arrow.gif" width="8" height="7"> 
                  A/C Couple Deluxe (Double Bed, Bath-tub)</font></td>
                <td class="fnt"><font color="#000000">US $ 31</font></td>
                <td class="fnt"><font color="#000000">tk. 2,000/-</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font color="#000000"><img src="images/arrow.gif" width="8" height="7"> 
                  A/C Standard Double (Two Single Bed)</font></td>
                <td class="fnt"><font color="#000000">US $ 28</font></td>
                <td class="fnt"><font color="#000000">tk. 1800/-</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font color="#000000"><img src="images/arrow.gif" width="8" height="7"> 
                  A/C Single Room </font></td>
                <td class="fnt"><font color="#000000">US $ 20 </font></td>
                <td class="fnt"><font color="#000000">tk. 1,300/-</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font color="#000000"><img src="images/arrow.gif" width="8" height="7"> 
                  Non A/C Double Room </font></td>
                <td class="fnt"><font color="#000000">US $ 19 </font></td>
                <td class="fnt"><font color="#000000">tk. 1,200/-</font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font color="#000000"><img src="images/arrow.gif" width="8" height="7"> 
                  Non A/C Single Room </font></td>
                <td class="fnt"><font color="#000000">US $ 14 </font></td>
                <td class="fnt"><font color="#000000">tk. 900/- </font></td>
              </tr>
              <tr> 
                <td class="pgraph"><font color="#000000"><img src="images/arrow.gif" width="8" height="7"> 
                  *Extra Bed </font></td>
                <td class="fnt"><font color="#000000">US $ 05</font></td>
                <td class="fnt"><font color="#000000"> tk. 250/-</font></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="30%" height="191" valign="top"> 
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td> 
            <table width="96%" border="1" cellspacing="0" cellpadding="0" align="right" bordercolor="#FFFFFF">
              <tr> 
                <td> 
                  <div align="right"><img src="images/best-rate.gif" width="275" height="98"></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><img src="images/logo_rp.gif" width="193" height="89"></div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="2" class="body"> 
      <div align="center">&copy; 2008-2009 Hotel Royal Palace.</div>
    </td>
  </tr>
  <tr>
    <td colspan="2" class="body">
      <div align="center">Development Powered by <a href="http://www.srishtyinteractive.net?cr=rp" target="_blank">srishtyinterActive</a></div>
    </td>
  </tr>
</table>
<p class="pgraph">&nbsp;</p>
</body>
</html>
