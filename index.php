<html>
<head>
<title>Royal Palace</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/body.css" type="text/css">
<link rel="stylesheet" href="prgaph.css" type="text/css">
<link rel="stylesheet" href="css/link.css" type="text/css">
<link rel="stylesheet" href="css/link.css" type="text/css">
</head>

<body bgcolor="#E7E1BD" text="#000000" leftmargin="0" topmargin="0">
<table width="850" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td colspan="2" height="32" bgcolor="#AB910C"> 
      <div align="center"> 
        <?php
	  	include"mnu_top.php";
	  ?>
      </div>
    </td>
  </tr>
  <tr> 
    <td colspan="2" class="body">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="15%" bgcolor="#FFFFFF"><img src="images/logo_rp.gif" width="193" height="89"></td>
          <td width="85%" valign="middle" bgcolor="#FFFFFF"> 
            <div align="right"><img src="images/wecare.jpg" width="317" height="36"></div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width="70%"><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="666" height="241">
        <param name=movie value="flash/intro.swf">
        <param name=quality value=high>
        <embed src="flash/intro.swf" quality=high pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="666" height="241">
        </embed> 
      </object></td>
    <td width="30%" valign="top"> 
      <div align="center"><img src="images/contact.jpg" width="270" height="242"></div>
    </td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td valign="top" height="450" width="70%"> 
      <table width="100%" border="0" cellspacing="0">
        <tr> 
          <td colspan="2" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr bgcolor="#AB910C"> 
          <td colspan="2"> 
            <div align="center"><font face="Garamond" size="6" color="#FFFFFF"><b><font size="5">Hotel 
              Royal Palace</font></b></font></div>
          </td>
        </tr>
        <tr> 
          <td height="48" colspan="2" bgcolor="#FFFFFF"><font color="#000000" face="Arial, Helvetica, sans-serif" size="2" class="pgraph">Hotel 
            Royal Palace Ltd. is one of the leading hotels in Dhaka that is known 
            for its well reputation. We have been leading the industry for last 
            12 years.</font></td>
        </tr>
        <tr> 
          <td height="181" width="18%"> 
            <p><img src="images/hotel_bd.jpg" width="118" height="176"></p>
          </td>
          <td height="181" width="82%" class="body" valign="middle" bgcolor="#FFFFFF"> 
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr>
                <td><font face="Arial, Helvetica, sans-serif" size="2" color="#000000"><span class="pgraph">Our 
                  place is located in the central point of Dhaka city, opposite 
                  of secretariats, adjacent to Motijheel C/A, five minutes walking 
                  distance from Supreme High Court, National Press Club, Baitul-Mokarram 
                  mosque. </span></font> 
                  <p class="pgraph"><font face="Arial, Helvetica, sans-serif" size="2" color="#000000" class="pgraph">We 
                    are within 25 minutes drive from the diplomatic zone of Gulshan 
                    &amp; Baridhara and 40 minutes drive from Zia International 
                    Airport!!</font></p>
                  <p class="pgraph"><font face="Arial, Helvetica, sans-serif" size="2" color="#000000" class="pgraph">We 
                    feel proud of our Royal Home as we have created an example 
                    of &#147;Well Reputed Hotel&#148; so long i.e. Our place is 
                    beyond different illegal activities &amp; incidents unlike 
                    others.</font></p>
          </td>
              </tr>
            </table>
            
          </td>
        </tr>
        <tr> 
          <td colspan="2" height="150"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" height="59" bgcolor="#E7E1BD">
              <tr> 
                <td width="52%" valign="middle" height="182" align="center" bgcolor="#FFFFFF"> 
                  <div align="center">
                    <table width="68%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td height="27" bgcolor="#AB910C"> 
                          <div align="center"><font color="#999999" size="3" face="Times New Roman, Times, serif"><b><font color="#000000">Conference 
                            Room</font></b></font></div>
                        </td>
                      </tr>
                      <tr> 
                        <td height="45"><img src="images/conference.jpg" width="264" height="118"></td>
                      </tr>
                      <tr>
                        <td height="91"><span class="pgraph">We tailor solutions 
                          to your specific needs &#150; from meetings, conferences, 
                          trade exhibitions and incentives for up to 100 delegates. 
                          For any event, grand or intimate, you&#146;re in professional 
                          hands at the HRP.</span></td>
                      </tr>
                    </table>
                  </div>
                </td>
                <td valign="top" height="182" width="48%" bgcolor="#FFFFFF"> 
                  <table width="81%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td height="29" bgcolor="#AB910C"> 
                        <div align="center"><font face="Times New Roman, Times, serif" size="3" color="#999999"><b><font color="#000000">Prayer 
                          Room</font></b></font></div>
                      </td>
                    </tr>
                    <tr> 
                      <td><img src="images/mosque.jpg" width="264" height="118"></td>
                    </tr>
                    <tr>
                      <td height="80"><span class="pgraph">At least at the time 
                        of Salaat you will hear the Azaan &amp; can conform Salaat 
                        within Jamaat here. We have the perfect, safe &amp; quiet 
                        environment for you &amp; your families&#146; comfortable 
                        stay.</span></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td colspan="2" height="2">&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="30%" height="450" valign="top"> 
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td> 
            <table width="96%" border="1" cellpadding="0" align="right" bordercolor="#FFFFFF">
              <tr> 
                <td> 
                  <div align="right"><img src="images/map.jpg" width="268" height="233"></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td><img src="images/best-rate.gif" width="275" height="98"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td colspan="2" class="body"> 
      <div align="center">&copy; 2008-2009 Hotel Royal Palace.</div>
    </td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td colspan="2" class="body" height="2"> 
      <div align="center">Development powered by : <a href="http://www.srishtyinteractive.net?cr=rp" target="_blank">srishty 
        interActive</a></div>
    </td>
  </tr>
</table>
</body>
</html>
