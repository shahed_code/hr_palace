<html>
<head>
<title>guest Book</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/body.css" type="text/css">
<link rel="stylesheet" href="prgaph.css" type="text/css">
<link rel="stylesheet" href="css/link.css" type="text/css">
</head>

<body bgcolor="#E7E1BD" text="#000000" leftmargin="0" topmargin="0" link="#990000">
<table width="881" border="0" align="center" cellpadding="0" cellspacing="0" height="188">
  <tr valign="middle" align="center" bgcolor="#AB910C"> 
    <td colspan="2" height="36" align="center> 
      
        
      <p class="pgraph" """""""> 
      <?php
			include"mnu_top.php";
		?>
    </td>
  </tr>
  <tr> 
    <td colspan="2" class="body" height="32"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="15%" bgcolor="#FFFFFF"><img src="images/logo_rp.gif" width="193" height="89"></td>
          <td width="85%" valign="middle" bgcolor="#FFFFFF"> 
            <div align="right"><img src="images/wecare.jpg" width="317" height="36"></div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width="70%" valign="middle" height="31" bgcolor="#AB910C"> 
      <div align="center"><font face="Garamond" size="5"><b><font color="#FFFFFF">Our 
        customers speak for us</font></b></font></div>
    </td>
    <td rowspan="2" valign="top" bgcolor="#FFFFFF"> 
      <div align="center"> </div>
      <table width="94%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
        <tr> 
          <td><img src="images/related_link.jpg" width="270" height="31"></td>
        </tr>
        <tr> 
          <td class="pgraph"> &nbsp; <img src="images/arrow.gif" width="8" height="7"> 
            <a href="tariff.php">Tariff</a> </td>
        </tr>
        <tr> 
          <td class="pgraph" height="15">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
            <a href="room_suits.php">Rooms &amp; Suits</a></td>
        </tr>
        <tr> 
          <td class="pgraph" height="17">&nbsp; <img src="images/arrow.gif" width="8" height="7"> 
            <a href="restaurent.php">Restaurant</a><b>&nbsp;</b></td>
        </tr>
        <tr> 
          <td class="pgraph" height="17">&nbsp;&nbsp;<img src="images/arrow.gif" width="8" height="7"> 
            <a href="facilities.php">Facilities</a> </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td valign="top" height="131" width="70%" class="pgraph"> 
      <p>&nbsp;</p>
      <p> </p>
      <p></p>
          
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="43">
        <tr> 
          <td height="20" class="pgraph"><font size="4">I have been a customer 
            of this hotel for last 6 years. It is the safest place in hotel industry 
            for Dhaka. I can be assured about the safety of my wife &amp; daughters, 
            family members at this place. The owner of this hotel is a well known 
            person in Chittagong. I really wish for the long term success of this 
            hotel&#146;</font></td>
        </tr>
        <tr> 
          <td> 
            <div align="right"><b>Mr. Jafrul Islam Chowdhury,</b></div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="right">Former Minister,</div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="right">Bangladesh National Party</div>
          </td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td height="33" class="pgraph"><font size="4">Hotel Royal Palace is 
            the best place to stay for the family persons because the internal 
            environment is very incredible&#146;-</font></td>
        </tr>
        <tr> 
          <td> 
            <div align="right"><b>Mr. Abdullah Al Noman, </b></div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="right">Former Minister,</div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="right">Bangladesh National Party</div>
          </td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td height="16" class="pgraph"><font size="4">The food of this hotel 
            is very tasty, good quality within your affordable price.&#146;-</font></td>
        </tr>
        <tr> 
          <td> 
            <div align="right"><b>Mr. Awal,</b></div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="right">Assistant Police Commissioner,</div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="right">Dhaka Metropolitan Police</div>
          </td>
        </tr>
      </table>
      <p></p>
    </td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td colspan="2" class="body" height="5"> 
      <div align="center">&copy; 2008-2009 Hotel Royal Palace.</div>
    </td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td colspan="2" class="body" height="2"> 
      <div align="center">Development powered by <font color="#000000">: <a href="http://www.srishtyinteractive.net?cr=rp" target="_blank">srishty 
        interActive</a></font></div>
    </td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td colspan="2" class="body" height="2">&nbsp;</td>
  </tr>
</table>
</body>
</html>
